# Introdução

### O que é?

TekLib é uma biblioteca (frontend) que contém funcionalidades comuns para atender aplicações Zeedhi Next construídas a partir do Teknisa CLI ( Produtos Teknisa ). 

### Para que serve?

A biblioteca vai prover a tela ( componentes ) e funcionalidades ( serviços ) para o ecossistema dos produtos teknisa.

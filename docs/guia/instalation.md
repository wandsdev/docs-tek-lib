# Instalação

```bash  
$ npm i @zeedhi/tek-lib  
```  

## Nota

\* Todo esse tutorial leva em consideração que o seu produto foi criado a partir do Teknisa CLI. Portanto seu produto já deve conter toda estrutura de arquivos e códigos que o ecossistema de produtos Teknisa necessita para um funcionamento correto.

\*\* A biblioteca TekLib contém um módulo de autenticação (componentes e serviços ), mas isso não descarta a necessidade de ter o módulo de login na sua aplicação, o mesmo contém telas de parametrizações que  são necessárias nos produtos teknisa e o backend se faz necessário.